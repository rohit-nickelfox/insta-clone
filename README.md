# Instagram Bakend

Built using modern technologies: node.js, express, mongoDB, mongoose.

## Environment Variables

To run this project, you will need to add the following environment variables to your config.env file

`PORT`: 3000

`DATABASE`:mongodb+srv://Rohit:<password>@cluster0.1yq93.mongodb.net/Insta-Clone?retryWrites=true&w=majority

`JWT_EXPIRES_IN`:90d

`EMAIL_USERNAME`:79e50635ac2601

`EMAIL_PASSWORD`:7e8287a681091a

`EMAIL_HOST`:smtp.mailtrap.io

`EMAIL_PORT`:25

## Description

In this project try to build **_Instagram backend_**

### Functionallity

In this project we have **_model_** **_controllers_** and **_routes_**

### model

In model folder we declared a **_userModel file_** and in this file declared all the fields of user like name email photo password and confirm password.

### controllers

In this folder we declared **_authController_** **_userController_** **_errorController_**

In **_authController_** we have authentication fields like **_signup_** **_login_** **_forgotPassword_** **_resetPassword_** **_updatePassword_**

In **_userController_** we can fetch user details and can uplaod photo

In **_errorController_** just try to control errors

### routes

In this folder we have **_userRoute_** defines **_requests are handled by the application endpoints_**

## Workin With API

### User

`localhost:3000/api/v1/users/signup` = (POST) To add new user in application.

`localhost:3000/api/v1/users/login` = (POST) Login with already existing account.

`localhost:3000/api/v1/users/forgotPassword` = (POST) Enter email id of existing account and in the response user will recieve new url to reset password.

`localhost:3000/api/v1/users/resetPassword/:token` = (PATCH) Use the url which you have recieved in email and now reset the password with new password details.

`localhost:3000/api/v1/users/updateMyPassword` = (PATCH) To update current logged in user password.

`localhost:3000/api/v1/users` = (GET) To get all users.

`localhost:3000/api/v1/users?:id` = (GET) get single user.

## How to Install all dependencies

**_Just write npm install on your terminal_**

## After that starts server

**_nodemon server_**
